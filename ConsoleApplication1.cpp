﻿#include <iostream>
#include <time.h>

int main()
{
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	int rem = buf.tm_mday % 5;
	int sum = 0;
	int const N = 5;
	int i;
	int j;
	int array[N][N];
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
		{
			array[i][j] = i + j;
			std::cout << array[i][j];
		}
		std::cout << "\n";
	}
	for (j = 0; j < N; j++)
	{
		sum = sum + array[rem][j];
	}
	std::cout <<"Sum of the " << rem << " " << "line = " << sum;
	return 0;
}